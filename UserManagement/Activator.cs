﻿using Microsoft.Extensions.DependencyInjection;
using UserManagement.Contract;

namespace UserManagement
{
    public class Activator
    {
        public static void AddServices(IServiceCollection services)
        {
            services.AddScoped<IUserWorkflow, UserWorkflow>();
            /*
            services.AddScoped<IRoleWorkflow, RoleWorkflow>();
            services.AddScoped<IPermissionWorkflow, PermissionWorkflow>();
            services.AddScoped<IMembershipWorkflow, MembershipWorkflow>();
            services.AddScoped<IInstallUserManagement, InstallUserManagement>();
            services.AddScoped<IGenericRepository<Role>, GenericRepository<Role>>();
            services.AddScoped<IGenericRepository<User>, GenericRepository<User>>();
            services.AddScoped<IGenericRepository<UserRole>, GenericRepository<UserRole>>();
            services.AddScoped<IGenericRepository<UserSession>, GenericRepository<UserSession>>();
            services.AddScoped<IGenericRepository<RolePermission>, GenericRepository<RolePermission>>();
            services.AddScoped<IGenericRepository<Permission>, GenericRepository<Permission>>();
            services.AddScoped<IGenericRepository<Membership>, GenericRepository<Membership>>();
            services.AddScoped<IGenericRepository<MembershipPriceOption>, GenericRepository<MembershipPriceOption>>();
            services.AddScoped<IGenericRepository<Currency>, GenericRepository<Currency>>();*/
        }
    }
}
