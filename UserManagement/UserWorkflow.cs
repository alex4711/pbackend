﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using Core.Utility;
using UserManagement.Contract;
using System.Threading.Tasks;
using Infrastructure.DataModel._Shared;
using Infrastructure.DataModel.UserManagement;
using Core.Framework.Contract;
using RestSharp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using RestSharp.Authenticators;

namespace UserManagement
{
        public class UserWorkflow : IUserWorkflow
        {
            private readonly ILogger<UserWorkflow> logger;
            private readonly SessionStorage sessionStorage;
            private readonly IUserRepository userRepository;
            private readonly ITemplateManager templateManager;
            private readonly IConfiguration config;
            public UserWorkflow(ILogger<UserWorkflow> logger, ITemplateManager templateManager, IUserRepository userRepository, SessionStorage sessionStorage, IConfiguration config)
            { 
                this.logger = logger;
                this.sessionStorage = sessionStorage;
                this.userRepository = userRepository;
                this.templateManager = templateManager;
                this.config = config;
                this.templateManager.SetTemplatePath(this.config.AsEnumerable().Single(x => x.Key.Equals("TemplateManager:Path")).Value);
            }

            public async Task<User> GetById(int id)
            {
                User userAuth = sessionStorage.GetUser();
                if (userAuth?.Id != id)
                {
                    throw new ApplicationException("Access forbidden. You can only access your own user.");
                }

                var user = await userRepository.GetById(id);
                if (user == null)
                {
                    throw new ApplicationException("User Id not found.");
                }

                user.PasswordHash = null;
                return user;
            }

            public async ValueTask<User> Create(User user)
            {
                if (!new EmailAddressAttribute().IsValid(user.Email))
                {
                    throw new ValidationException("Email is invalid.");
                }

                if (!IsUserNameAllowed(user.Username))
                {
                    throw new ValidationException("Username is invalid. Username may contain letters, numbers or '.','-' or '_'.");
                }

                if (!IsPasswordAllowed(user.PasswordHash))
                {
                    throw new ValidationException(
                        "Password is invalid. Password must be 8-30 characters long and contain letters (lower and uppercase) and numbers.");
                }

                user.Email = user.Email.ToLower();
                user.Username = user.Username.ToLower();

                User userFound = await userRepository.GetByUsername(user.Email);
                if (userFound != null)
                {
                    throw new ValidationException("Email already exists.");
                }

                userFound = await userRepository.GetByUsername(user.Username);
                if (userFound != null)
                {
                    throw new ValidationException("Username already exists.");
                } 
                
                user.PasswordHash = CryptoUtility.GenerateHashAndSalt(user.PasswordHash);
                user.ApiKey = CryptoUtility.RandomMd5();

                SendActivationLinkToUser(user);

                User userResult = await userRepository.Create(user);
                userResult.PasswordHash = null;
                return userResult;
            }

            public async Task<UserSession> Login(string username, string password)
            {
                User userFound = await userRepository.GetByUsername(username);

                UserSession userSession = new UserSession()
                {
                    SessionId = "",
                    RemoteAddress = "",
                    UserAgent = ""
                };

                if (userFound == null || !CryptoUtility.ValidateHash(password, userFound.PasswordHash))
                {
                    throw new ValidationException("Username or password is not valid.");
                }

                userSession.SessionId = CryptoUtility.RandomMd5();
                userSession.UserId = userFound.Id;
                return await userRepository.CreateSession(userSession);
            }

            public async Task<bool> Logout()
            {
                UserSession userSession = sessionStorage.GetUserSession(); 
                return await userRepository.DeleteSession(userSession.SessionId);
            }

            public async Task<bool> Delete(int id)
            {
                User userAuth = sessionStorage.GetUser();
                if (userAuth.Id != id)
                {
                    throw new ApplicationException("Access forbidden. You can only access your own user.");
                }

                return await userRepository.Delete(id);
            }

            public async Task<User> AuthenticateByApiKey(string apikey)
            { 
                User user = await userRepository.GetByApiKey(apikey);

                if (user != null)
                {
                    sessionStorage.SetUser(user);
                    sessionStorage.SetUserSession(null);
                    return user;
                }

                return null;
            }

            public async Task<User> AuthenticateBySessionId(string sessionid)
            {
                UserSession userSession = await userRepository.GetBySessionId(sessionid);

                if (userSession != null)
                {
                    sessionStorage.SetUser(userSession.User);
                    sessionStorage.SetUserSession(userSession);
                    return userSession.User;
                }

                return null;
            }

            private bool IsUserNameAllowed(string userName)
            {
                Regex userNameAllowedRegEx = new Regex(@"^[a-zA-Z]{1}[a-zA-Z0-9\._\-]{0,23}[^.-]$", RegexOptions.Compiled);
                Regex userNameIllegalEndingRegEx = new Regex(@"(\.|\-|\._|\-_)$", RegexOptions.Compiled);
                if (string.IsNullOrEmpty(userName)
                    || !userNameAllowedRegEx.IsMatch(userName)
                    || userNameIllegalEndingRegEx.IsMatch(userName))
                {
                    return false;
                }

                return true;
            }

            private bool IsPasswordAllowed(string userName)
            {
                Regex passwordAllowedRegEx = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,30}$",
                    RegexOptions.Compiled);
                if (string.IsNullOrEmpty(userName)
                    || !passwordAllowedRegEx.IsMatch(userName))
                {
                    return false;
                }

                return true;
            }

            private string GenerateVerificationLink(User user)
            {
                string baseProtocol = config.AsEnumerable().Single(x => x.Key.Equals("General:BaseProtocol")).Value;
                string baseUrl = config.AsEnumerable().Single(x => x.Key.Equals("General:BaseUrl")).Value;
                return $"{baseProtocol}://{baseUrl}/api/UserManagement/VerifyEmail?uid={user.Id}&v={GenerateVerificationKey(user)}";
            }

            private string GenerateVerificationKey(User user)
            {
                return CryptoUtility.CreateMd5(user.Id + user.Email + "emailverification");
            }

            private void SendActivationLinkToUser(User user)
            {
                string domain = config.AsEnumerable().Single(x => x.Key.Equals("Mailgun:Domain")).Value;

                RestClient client = new RestClient
                {
                    BaseUrl = new Uri(config.AsEnumerable().Single(x => x.Key.Equals("Mailgun:Url")).Value),
                    Authenticator = new HttpBasicAuthenticator("api",
                        config.AsEnumerable().Single(x => x.Key.Equals("Mailgun:ApiKey")).Value)
                };

                RestRequest request = new RestRequest();
                request.AddParameter("domain", domain,
                    ParameterType.UrlSegment);
                request.Resource = $"{domain}/messages";
                request.AddParameter("from", config.AsEnumerable().Single(x => x.Key.Equals("Mailgun:From")).Value);
                request.AddParameter("to", user.Email);
                request.AddParameter("subject", templateManager.GetEmailSubject("User", "EmailNewUser"));
                request.AddParameter("text", templateManager.GetEmailbody("User", "EmailNewUser").Replace("[[username]]", user.Username).Replace("[[link]]", GenerateVerificationLink(user)));
                request.Method = Method.POST;
                IRestResponse response = client.Execute(request);
                logger.LogInformation($"User created and new email send to: {user.Email} Mailgun API response: {response.Content.Substring(0, response.Content.Length > 500 ? 500 : response.Content.Length)}");
            }
        }
    }

