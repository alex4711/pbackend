﻿using Core.Framework.Contract;
using System;
using System.IO;
using Microsoft.Extensions.Caching.Memory;

namespace Core.Framework
{
    public class TemplateManager : ITemplateManager
    {
        private readonly IMemoryCache cache;
        private string templatePath;
        public TemplateManager(IMemoryCache cache)
        {
            this.cache = cache;
        }

        public string GetEmailSubject(string category, string key)
        {
            string text = Get(category, key);
            if (text.IndexOf(Environment.NewLine, StringComparison.Ordinal) > -1)
            {
                string firstline = text.Substring(0, text.IndexOf(Environment.NewLine, StringComparison.Ordinal));
                if (firstline.StartsWith("Subject:"))
                {
                    string subject = firstline.Replace("Subject: ", "");
                    if (!string.IsNullOrWhiteSpace(subject))
                    {
                        return subject;
                    }
                }
            }

            return "";
        }

        public string GetEmailbody(string category, string key)
        {
            string text = Get(category, key);
            if (text.IndexOf(Environment.NewLine, StringComparison.Ordinal) > -1)
            {
                string firstline = text.Substring(0, text.IndexOf(Environment.NewLine, StringComparison.Ordinal));
                if (firstline.StartsWith("Subject:"))
                {
                    int lineEndPos = text.IndexOf(Environment.NewLine, StringComparison.Ordinal) + 1;
                    return text.Substring(lineEndPos, text.Length - lineEndPos);
                }
            }

            return text;
        }

        public string Get(string category, string key)
        {
            return cache.GetOrCreate<string>($"TemplateManager:{category}:{key}",
            cacheEntry => {
                cacheEntry.SlidingExpiration = TimeSpan.FromDays(3);

                string categoryPath = Path.Combine(templatePath, category);
                if (!Directory.Exists(templatePath))
                {
                    throw new DirectoryNotFoundException($"Template path {templatePath} does not exists.");
                }
                if (!Directory.Exists(categoryPath))
                {
                    throw new DirectoryNotFoundException($"Template category path {categoryPath} does not exists.");
                }

                string fileNonOrigin = Path.Combine(categoryPath, $"{key}.txt");
                string fileOrigin = Path.Combine(categoryPath, $"{key}.origin.txt");
                if (File.Exists(fileNonOrigin))
                {
                    return File.ReadAllText(fileNonOrigin);
                }
                if (File.Exists(fileOrigin))
                {
                    return File.ReadAllText(fileOrigin);
                }

                throw new FileNotFoundException($"Template file {key}.txt or {key}.origin.txt does not exists.");
            });
        }

        public void SetTemplatePath(string path)
        {
            templatePath = path;
        }
    }
}
