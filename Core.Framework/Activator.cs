﻿using Core.Framework.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Framework
{
    public class Activator
    {
        public static void AddServices(IServiceCollection services)
        {
            services.AddScoped<ITemplateManager, TemplateManager>();
        }
    }
}
