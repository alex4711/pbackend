﻿using Infrastructure.DataModel.UserManagement;

namespace Infrastructure.DataModel._Shared
{
    public class SessionStorage
    {
        private User user;
        private UserSession userSession;

        public void SetUser(User userParameter)
        {
            user = userParameter;
        }

        public User GetUser()
        {
            return user;
        }

        public void SetUserSession(UserSession userSessionParameter)
        {
            userSession = userSessionParameter;
        }

        public UserSession GetUserSession()
        {
            return userSession;
        }
    }
}
