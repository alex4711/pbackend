﻿using System;

namespace Infrastructure.DataModel._Shared
{
    public class Entity
    {
        public DateTime? CreatedAt { get; set; }
        public string CreatedFrom { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedFrom { get; set; }
    }
}
