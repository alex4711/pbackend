﻿using System.Collections.Generic;
using Infrastructure.DataModel._Shared;

namespace Infrastructure.DataModel.UserManagement
{
    public class User : Entity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string ApiKey { get; set; }
        public string PasswordHash { get; set; }
        public ICollection<UserSession> UserSessions { get; set; }
    }
}
