﻿using Infrastructure.DataModel._Shared;

namespace Infrastructure.DataModel.UserManagement
{
    public class UserSession : Entity
    {
        public string SessionId { get; set; }
        public int UserId { get; set; }
        public string UserAgent { get; set; }
        public string RemoteAddress { get; set; }
        public virtual User User { get; set; }
    }
}
