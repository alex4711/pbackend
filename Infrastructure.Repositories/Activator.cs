﻿using Infrastructure.Repositories.UserManagement;
using Microsoft.Extensions.DependencyInjection;
using UserManagement.Contract;

namespace Infrastructure.Repositories
{
    public class Activator
    {
        public static void AddServices(IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRespository>();
        }
    }
}
