﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.DataModel._Shared;
using Infrastructure.DataModel.UserManagement;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class AppDbContext : DbContext
    {
        private readonly SessionStorage session;

        #region UserManagement
        //public DbSet<User> Users { get; set; }
        //public DbSet<UserSession> UserSessions { get; set; }
        #endregion


        //public DbSet<DatabaseConfigurationItem> DatabaseConfigurationItems { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options, SessionStorage session) : base(options)
        {
            this.session = session;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            /*
            modelBuilder.Entity<RolePermission>()
                .HasKey(bc => new { bc.PermissionId, bc.RoleId });

            modelBuilder.Entity<RolePermission>()
                .HasOne(bc => bc.Permission)
                .WithMany(c => c.RolePermissions)
                .HasForeignKey(bc => bc.PermissionId);

            modelBuilder.Entity<RolePermission>()
                .HasOne(bc => bc.Role)
                .WithMany(b => b.RolePermissions)
                .HasForeignKey(bc => bc.RoleId);


            modelBuilder.Entity<UserRole>()
                .HasKey(bc => new { bc.RoleId, bc.UserId });

            modelBuilder.Entity<UserRole>()
                .HasOne(bc => bc.Role)
                .WithMany(b => b.UserRoles)
                .HasForeignKey(bc => bc.RoleId);

            modelBuilder.Entity<UserRole>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.UserRoles)
                .HasForeignKey(bc => bc.UserId);
                */
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e =>
                    e.State == EntityState.Added
                    || e.State == EntityState.Modified);

            foreach (var entityEntry in entries)
            {
                if (entityEntry.Metadata.FindProperty("CreatedAt") != null)
                {
                    if (entityEntry.State == EntityState.Added)
                    {
                        entityEntry.Property("CreatedAt").CurrentValue = DateTime.Now;
                        entityEntry.Property("CreatedFrom").CurrentValue = session.GetUser()?.Username;
                    }
                    else
                    {
                        entityEntry.Property("UpdatedAt").CurrentValue = DateTime.Now;
                        entityEntry.Property("UpdatedFrom").CurrentValue = session.GetUser()?.Username;
                    }
                }
            }

            return (await base.SaveChangesAsync(true, cancellationToken));
        }



        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e =>
                    e.State == EntityState.Added
                    || e.State == EntityState.Modified);

            foreach (var entityEntry in entries)
            {
                if (entityEntry.Metadata.FindProperty("CreatedAt") != null)
                {
                    if (entityEntry.State == EntityState.Added)
                    {
                        entityEntry.Property("CreatedAt").CurrentValue = DateTime.Now;
                        entityEntry.Property("CreatedFrom").CurrentValue = session.GetUser()?.Username;
                    }
                    else
                    {
                        entityEntry.Property("UpdatedAt").CurrentValue = DateTime.Now;
                        entityEntry.Property("UpdatedFrom").CurrentValue = session.GetUser()?.Username;
                    }
                }
            }

            return base.SaveChanges();
        }
    }
}
