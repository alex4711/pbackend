﻿using Infrastructure.DataModel.UserManagement;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Repositories.UserManagement
{
    public class UserSessionConfiguration : IEntityTypeConfiguration<UserSession>
    {
        public void Configure(EntityTypeBuilder<UserSession> builder)
        {
            builder.ToTable("usermanagement_user_session"); // or [Table("porndb_user")]
            builder.HasKey(k => k.SessionId);
            builder.Property(p => p.SessionId)
                .HasMaxLength(32)
                .IsRequired();
            builder.Property(p => p.UserAgent)
                .HasMaxLength(255);
            builder.Property(p => p.RemoteAddress)
                .HasMaxLength(45);
            //builder.HasMany(x => x.U)
            /*
            builder.HasKey(0 => o.OrderNumber);
            builder.Property(t => t.OrderDate)
                .IsRequired()
                .HasColumnType("Date")
                .HasDefaultValueSql("GetDate()"
                */
        }
    }
}
