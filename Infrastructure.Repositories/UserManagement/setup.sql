﻿CREATE TABLE usermanagement_user (
	Id int IDENTITY(1,1) NOT NULL,
	Username varchar(60) NOT NULL,
	Email varchar(255) NULL,
	ApiKey varchar(32) NULL,
	PasswordHash varchar(255) NULL,
	-- default fields:
	CreatedAt datetime2(7) NULL,
	CreatedFrom varchar(60) NULL,
	UpdatedAt datetime2(7) NULL,
	UpdatedFrom varchar(60) NULL,
	CONSTRAINT PK_usermanagement_user PRIMARY KEY (Id ASC)
);

CREATE TABLE usermanagement_user_session (
	SessionId varchar(32) NOT NULL,
	UserId int NOT NULL,
	UserAgent varchar(255) NULL,
	RemoteAddress varchar(45) NULL,
	-- default fields:
	CreatedAt datetime2(7) NULL,
	CreatedFrom varchar(60) NULL,
	UpdatedAt datetime2(7) NULL,
	UpdatedFrom varchar(60) NULL,
	CONSTRAINT PK_usermanagement_user_session PRIMARY KEY (SessionId ASC)
);