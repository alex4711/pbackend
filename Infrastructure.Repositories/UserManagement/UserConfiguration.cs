﻿using Infrastructure.DataModel.UserManagement;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Repositories.UserManagement
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("usermanagement_user"); // or [Table("porndb_user")]
            builder.HasMany(x => x.UserSessions).WithOne(u => u.User);
            builder.Property(p => p.Username)
                .HasMaxLength(60)
                .IsRequired();
            builder.Property(p => p.Email)
                .HasMaxLength(255);
            /*
            builder.HasKey(0 => o.OrderNumber);
            builder.Property(t => t.OrderDate)
                .IsRequired()
                .HasColumnType("Date")
                .HasDefaultValueSql("GetDate()"
                */
        }
    }
}
