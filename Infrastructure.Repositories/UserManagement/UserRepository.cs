﻿using System;
using System.Linq;
using Infrastructure.DataModel.UserManagement;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using UserManagement.Contract;

namespace Infrastructure.Repositories.UserManagement
{
    public class UserRespository : IUserRepository
    {
        private readonly AppDbContext dbContext;
        public UserRespository(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        //public IQueryable<User> Query => dbContext.Set<User>().AsQueryable().AsNoTracking();

        public async ValueTask<User> Create(User user)
        {
            var result = dbContext.Set<User>().AddAsync(user);
            await dbContext.SaveChangesAsync();
            return result.Result.Entity;
        }

        public async ValueTask<UserSession> CreateSession(UserSession session)
        {
            var result = dbContext.Set<UserSession>().AddAsync(session);
            await dbContext.SaveChangesAsync();
            return result.Result.Entity;
        }

        public async Task<bool> DeleteSession(string sessionId)
        {
            var element = await dbContext.Set<UserSession>().FindAsync(sessionId);
            dbContext.Set<UserSession>().Remove(element);
            return (await dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Update(User user)
        {
            //dbContext.Set<User>().Where(x => x.ApiKey.Equals("")).ToListAsync();
            dbContext.Set<User>().Update(user);
            return (await dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Delete(int id)
        {
            //Context.Entry(await Context.MyDbSet.FirstOrDefaultAsync(x => x.Id == item.Id)).CurrentValues.SetValues(item);
            //return (await Context.SaveChangesAsync()) > 0;

            var element = await dbContext.Set<User>().FindAsync(id);
            dbContext.Set<User>().Remove(element);
            return (await dbContext.SaveChangesAsync()) > 0;
        }

        public async ValueTask<User> GetByApiKey(string apiKey)
        {
            return await dbContext.Set<User>().FirstOrDefaultAsync(x => x.ApiKey.Equals(apiKey));
        }

        public async ValueTask<User> GetById(int id)
        {
            return await dbContext.Set<User>().FindAsync(id); //.AsQueryable().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async ValueTask<User> GetByUsername(string username)
        {
            return await dbContext.Set<User>().FirstOrDefaultAsync(x => x.Email.Equals(username) || x.Username.Equals(username));
        }

        public async ValueTask<UserSession> GetBySessionId(string sessionId)
        {
            return await dbContext.Set<UserSession>().Include(x => x.User).FirstOrDefaultAsync(x => x.SessionId.Equals(sessionId));
        }
    }
}
