﻿using Infrastructure.DataModel.UserManagement;
using System.Threading.Tasks;

namespace UserManagement.Contract
{
    public interface IUserRepository
    {
        ValueTask<User> Create(User user);
        ValueTask<UserSession> CreateSession(UserSession session);
        Task<bool> Update(User user);
        Task<bool> Delete(int id);
        Task<bool> DeleteSession(string sessionId);
        ValueTask<User> GetById(int id);
        ValueTask<User> GetByUsername(string username);
        ValueTask<User> GetByApiKey(string apiKey);
        ValueTask<UserSession> GetBySessionId(string sessionId);
    }
}
