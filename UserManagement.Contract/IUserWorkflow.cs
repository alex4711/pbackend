﻿using System.Threading.Tasks;
using Infrastructure.DataModel.UserManagement;

namespace UserManagement.Contract
{
    public interface IUserWorkflow
    {
        ValueTask<User> Create(User user);

        Task<User> GetById(int id);

        Task<bool> Delete(int id);

        Task<UserSession> Login(string username, string password);

        Task<User> AuthenticateByApiKey(string apikey);

        Task<User> AuthenticateBySessionId(string sessionid);

        Task<bool> Logout();
    }
}
