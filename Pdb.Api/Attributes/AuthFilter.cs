﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;
using Infrastructure.DataModel.UserManagement;
using UserManagement.Contract;

namespace Pdb.Api.Attributes
{
    public class AuthFilter : ActionFilterAttribute
    {
        public override async Task OnActionExecutionAsync(ActionExecutingContext context,
            ActionExecutionDelegate next)
        {

            // logic before action goes here

            IUserWorkflow userWorkflow = (IUserWorkflow) context.HttpContext.RequestServices.GetService(typeof(IUserWorkflow));
            IMemoryCache cache = (IMemoryCache) context.HttpContext.RequestServices.GetService(typeof(IMemoryCache));

            User authorizedUser = null;
            if (context.HttpContext.Request.Headers.ContainsKey("X-APIKEY"))
            {
                string apiKey = context.HttpContext.Request.Headers["X-APIKEY"];
                string cacheKey = $"apikey:{apiKey}";
                if (!cache.TryGetValue(cacheKey, out authorizedUser))
                {
                    authorizedUser = await userWorkflow.AuthenticateByApiKey(apiKey);
                    if (authorizedUser != null)
                    {
                        cache.Set(cacheKey, authorizedUser, new MemoryCacheEntryOptions()
                            .SetSlidingExpiration(TimeSpan.FromSeconds(1800)));
                    }
                }
            }
            else if (context.HttpContext.Request.Headers.ContainsKey("X-SESSIONID"))
            {
                string sessionid = context.HttpContext.Request.Headers["X-SESSIONID"];
                string cacheKey = $"sessionid:{sessionid}";
                if (!cache.TryGetValue(cacheKey, out authorizedUser))
                {
                    authorizedUser = await userWorkflow.AuthenticateBySessionId(sessionid);
                    if (authorizedUser != null)
                    {
                        cache.Set(cacheKey, authorizedUser, new MemoryCacheEntryOptions()
                            .SetSlidingExpiration(TimeSpan.FromSeconds(1800)));
                    }
                }
            }

            if (authorizedUser == null) 
            {
                context.Result = new UnauthorizedObjectResult(new { error = "Invalid authorization." });
                return;
            }
            /*
            IPermissionWorkflow permissionWorkflow = (IPermissionWorkflow)context.HttpContext.RequestServices.GetService(typeof(IPermissionWorkflow));
            List<string> requiredPermissionList = new List<string>();
            // Search class for permission attributes
            context.Controller.GetType().GetCustomAttributesData()
                .Where(x => x.AttributeType == typeof(PermissionRequiredAttribute)).ToList().ForEach(x =>
                {
                    string s = x.ConstructorArguments.First().Value.ToString();
                    if (!requiredPermissionList.Contains(s))
                    {
                        requiredPermissionList.Add(s);
                    }
                });

            string actionName = ((ControllerBase)context.Controller)
                .ControllerContext.ActionDescriptor.ActionName;
            // Search method for permission attributes
            context.Controller.GetType().GetMethods().Where(m => m.Name.Equals(actionName) && m.GetCustomAttributes(typeof(PermissionRequiredAttribute), false).Length > 0)
                .SelectMany(t => t.GetCustomAttributesData())
                .Where(x => x.AttributeType == typeof(PermissionRequiredAttribute)).ToList()
                .ForEach(x =>
                {
                    string s = x.ConstructorArguments.First().Value.ToString();
                    if (!requiredPermissionList.Contains(s))
                    {
                        requiredPermissionList.Add(s);
                    }
                });

            await foreach (Permission permission in permissionWorkflow.GetByUserId(authorizedUser.Id))
            {
                if (requiredPermissionList.Contains(permission.Name))
                {
                    requiredPermissionList.Remove(permission.Name);
                }
            }

            if (requiredPermissionList.Any())
            {
                context.Result = new BadRequestObjectResult(new { error = "Permission denied (Required permissions: " + string.Join(", ", requiredPermissionList) + ")" });
                return;
            }
            */

            // the actual action
            await next();

            // logic after the action goes here
        }
    }
}
