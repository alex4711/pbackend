﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Pdb.Api.Attributes
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected

            //if (ex is MyNotFoundException) code = HttpStatusCode.NotFound;
            //else if (ex is MyUnauthorizedException) code = HttpStatusCode.Unauthorized;
            if (ex is ValidationException)
            {
                code = HttpStatusCode.BadRequest;
                Log.Information($"HTTP \"{context.Request.Method}\" \"{context.Request.Path}\" responded {(int)code} with \"{ex.Message}\"");
            }

            /*
            if (ex is ElementNotFoundException)
            {
                code = HttpStatusCode.NotFound;
                Log.Information($"HTTP \"{context.Request.Method}\" \"{context.Request.Path}\" responded {(int)code} with \"{ex.Message}\"");
            }
            */

            var result = JsonSerializer.Serialize(new { error = ex.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
