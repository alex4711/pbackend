﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;

namespace Pdb.Api.Attributes
{
    public class SupportAuthFilter : ActionFilterAttribute
    {
        public override async Task OnActionExecutionAsync(ActionExecutingContext context,
            ActionExecutionDelegate next)
        {

            // logic before action goes here

            IConfiguration configuration = (IConfiguration)context.HttpContext.RequestServices.GetService(typeof(IConfiguration));
            //SessionStorage sessionStorage = (SessionStorage)context.HttpContext.RequestServices.GetService(typeof(SessionStorage));

            bool isAuthorized = false;
            if (context.HttpContext.Request.Headers.ContainsKey("X-SUPPORT-APIKEY"))
            {
                string installerApiKey = configuration.AsEnumerable().Single(x => x.Key.Equals("Support:ApiKey")).Value;
                if (installerApiKey.Equals(context.HttpContext.Request.Headers["X-SUPPORT-APIKEY"]))
                {
                    isAuthorized = true;
                    /*
                    sessionStorage.SetUser(new User()
                    {
                        Username = "Installer"
                    });
                    sessionStorage.SetUserSession(null);*/
                }
            }

            if (!isAuthorized)
            {
                context.Result = new UnauthorizedObjectResult(new { error = "Invalid Installer authorization." });
                return;
            }


            // the actual action
            await next();

            // logic after the action goes here
        }
    }
}
