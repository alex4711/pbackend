﻿using System.Threading.Tasks;
using Infrastructure.DataModel.UserManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Pdb.Api.Attributes;
using UserManagement.Contract;

namespace Pdb.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserManagementController : ControllerBase
    {
        private readonly ILogger<UserManagementController> logger;
        private readonly IConfiguration config;
        private readonly IUserWorkflow userWorkflow;

        public UserManagementController(ILogger<UserManagementController> logger, IConfiguration config, IUserWorkflow userWorkflow)
        {
            this.logger = logger;
            this.config = config;
            this.userWorkflow = userWorkflow;
        }

        //[SupportAuthFilter]
        [ActionName("Create")]
        [HttpPost()]
        public async ValueTask<User> Create([FromBody] User user)
        {
            return await userWorkflow.Create(user);
        }

        [AuthFilter]
        [ActionName("Delete")]
        [HttpGet()]
        public async ValueTask<bool> Delete([FromQuery] int id)
        {
            return await userWorkflow.Delete(id);
        }
    }
}
