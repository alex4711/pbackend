﻿namespace Core.Framework.Contract
{
    public interface ITemplateManager
    {
        string GetEmailSubject(string category, string key);
        string GetEmailbody(string category, string key);
        string Get(string category, string key);
        void SetTemplatePath(string path);
    }
}
